More and more healthcare processes are being supported by electronic devices such as computers (eHealth) and new, previously unthinkable healthcare processes are emerging, driven by digitalisation (dHealth). In order to better connect these two worlds, tools need to be created that can be found here. 

## Contributing and Support

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for contributing to cdeHealth projects which are hosted in the [cdeHealth group](https://gitlab.com/cdehealth) on GitLab.com.
Please read [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md) to make participation in our community a harassment free experience for everyone.

# The cdeHealth Pipeline

As a example of usage of these tools, a common pipeline is introduced. It converts health data from one form to another, documents the changes by git and makes the original and converted data accessible by git server api and a additional FHIR® server api, if FHIR® is part of the conversion chain.

[![cdeHealth pipeline](cdeHealth_pipeline.drawio.png){: style="width: 100%"}](cdeHealth_pipeline.drawio.png)

## Authors and acknowledgment
We want to thank
- ELGA GmbH with their CDA2FHIR projects and
- AIT Austrian Institute of Technology GmbH with their SmartFOX project.

### License and Legal Terms
HL7®, HEALTH LEVEL SEVEN® and FHIR® are trademarks owned by Health Level Seven International, registered with the United States Patent and Trademark Office.

Projects in this gitlab group contain and reference intellectual property owned by third parties (“Third Party IP”). Acceptance of these License Terms does not grant any rights with respect to Third Party IP. The licensee alone is responsible for identifying and obtaining any necessary licenses or authorizations to utilize Third Party IP in connection with the specification or otherwise.

Please look into each project for their individual licences.